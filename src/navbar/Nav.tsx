import './Nav.css'
import './Button.css'
function Nav() {
    return (
        <nav className="navbar navbar-expand-lg">
            <div className="container">
                <div className="row justify-content-end">
                    <div className="col-2 d-flex justify-content-center">
                        <div className="neon-button">
                            Sijie
                        </div>
                    </div>
                    <div className="col-2 d-flex justify-content-center">
                        <div className="neon-button">
                            May
                        </div>
                    </div>
                    <div className="col-2 d-flex justify-content-center">
                        <div className="neon-button">
                            Blog
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default Nav;
