import './App.css';
import GameOfLife from './canvas/GameOfLife';
import Nav from './navbar/Nav';

function App() {
    return (
        <>
            <GameOfLife/>
            <Nav/>
        </>
    );
}


export default App;
