import React, {useEffect, useRef, useState} from 'react';
import Board from "./Board"
import {debounce} from "./Utils";
import "./Canvas.css"

function GameOfLife() {
  const canvasRef = useRef(null);
  const [, setBoard] = useState(null);

  useEffect(() => {
    // Assuming Board class is already defined and accepts the canvas DOM element
    const newBoard = new Board(canvasRef.current, 5, {bg: [0, 0, 0, 1], alive: [200, 80, 20, 0.5]});
    setBoard(newBoard);
    newBoard.init();
    newBoard.start();

    // Define the resize handler
    const reset = debounce(() => {
      newBoard.destroy();
      newBoard.init();
      newBoard.start();
    }, 250);

    // Add event listener for window resize
    window.addEventListener('resize', reset);

    // Clean up
    return () => {
      window.removeEventListener('resize', reset);
      newBoard.destroy();
    };
  }, []);

  return <canvas ref={canvasRef} id="gol"/>;
}

export default GameOfLife;