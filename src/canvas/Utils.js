export function hsla (h, s, l, a) {
  return `hsla(${h}, ${s}%, ${l}%, ${a})`
}

export function map (value, in1, in2, out1, out2) {
  return (value - in1) * (out2 - out1) / (in2 - in1) + out1;
}

export function debounce(func, wait, immediate) {
  let timeout;
  return function() {
    const context = this, args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}
