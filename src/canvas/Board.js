import {hsla} from "./Utils";
import Cell from "./Cell";

class Board {
  constructor (canvas, scale, colors) {
    this.bg = hsla(...colors.bg)
    this.cells = []
    this.colors = colors
    this.ctx = canvas.getContext('2d')
    this.scale = scale
    this.dragging = false
    this.handlers = {
      handleMouseDown: this.handleMouseDown.bind(this),
      handleMouseMove: this.handleMouseMove.bind(this),
      handleMouseUp: this.handleMouseUp.bind(this)
    }
  }

  init () {
    const bg = this.colors.bg.slice(0)
    bg[3] = 1

    this.setupCanvas()
    this.setupBoard()
    this.setupCells()
    this.setupListeners()

    this.ctx.fillStyle = hsla(...bg)
    this.ctx.fillRect(0, 0, this.w, this.h)
  }

  draw () {
    const max = this.cells.length

    this.ctx.fillStyle = this.bg
    this.ctx.fillRect(0, 0, this.w, this.h)

    for (let i = 0; i < max; i++) {
      this.cells[i].draw()
    }
  }

  setupCanvas () {
    this.ctx.canvas.width = this.ctx.canvas.clientWidth
    this.ctx.canvas.height = this.ctx.canvas.clientHeight
    this.w = this.ctx.canvas.width + this.scale
    this.h = this.ctx.canvas.height + this.scale

    this.ctx.fillRect(0, 0, this.w, this.h)
  }

  setupBoard () {
    this.cellsLength = Math.floor((this.w / this.scale) * (this.h / this.scale))
    this.columns = Math.floor(this.w / this.scale)
    this.rows = Math.floor(this.h / this.scale)
  }

  setupCells () {
    const len = this.cellsLength

    // Instead of storing our cells in multiple nested arrays (x and y),
    // we can store them in one array and derive their X and Y values later.
    // This should result in an exponential performance gain, but I haven't
    // tested to verify.
    for (let i = 0; i < len; i++) {
      this.cells.push(new Cell(this, i))
    }

    for (let i = 0; i < len; i++) {
      this.cells[i].setColors()
    }
  }

  setupListeners () {
    this.ctx.canvas.addEventListener('mousedown', this.handlers.handleMouseDown)
    this.ctx.canvas.addEventListener('mouseup', this.handlers.handleMouseUp)
    this.ctx.canvas.addEventListener('mousemove', this.handlers.handleMouseMove)
  }

  drawAtCoords(clientX, clientY) {
    const x = Math.floor(clientX / this.scale)
    const y = Math.floor(clientY / this.scale)
    const i = (y * this.columns) + x
    const cell = this.cells[i]
    const neighbors = cell.neighbors

    cell.alive = true

    neighbors.forEach(neighbor => {
      this.cells[neighbor].alive = true
    })
  }

  handleMouseDown (e) {

    this.dragging = true
    this.drawAtCoords(e.clientX, e.clientY)
  }

  handleMouseUp () {

    this.dragging = false
  }

  handleMouseMove(e) {
    if (!this.dragging) {
      return
    }

    window.requestAnimationFrame(() => { this.drawAtCoords(e.clientX, e.clientY) })
  }

  destroy () {
    window.cancelAnimationFrame(this.handlers.raf)
    this.ctx.canvas.removeEventListener('mousedown', this.handlers.handleMouseDown)
    this.ctx.canvas.removeEventListener('mouseup', this.handlers.handleMouseUp)
    this.ctx.canvas.removeEventListener('mousemove', this.handlers.handleMouseMove)
    this.cells = []
    this.dragging = false
    this.handlers = {
      handleMouseDown: this.handleMouseDown.bind(this),
      handleMouseMove: this.handleMouseMove.bind(this),
      handleMouseUp: this.handleMouseUp.bind(this)
    }
  }

  update () {
    const max = this.cells.length

    for (let i = 0; i < max; i++) {
      this.cells[i].updatePrevious()
    }

    for (let i = 0; i < max; i++) {
      this.cells[i].update()
    }
  }

  step () {
    this.update()
    this.draw()
  }

  loop () {
    this.step()
    return window.requestAnimationFrame(this.loop.bind(this))
  }

  start () {
    this.handlers.raf = this.loop()
  }
}

export default Board;
