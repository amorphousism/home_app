import {hsla, map} from "./Utils";

class Cell {
  constructor (board, i) {
    this.alive = Math.random() < 0.1
    this.board = board
    this.i = i
    // X and Y are derivatives of the cell's index in the array.
    this.x = Math.floor(i % board.columns) * board.scale
    this.y = Math.floor(i / board.columns) * board.scale
    this.col = Math.floor(this.x / board.scale)
    this.row = Math.floor(this.y / board.scale)
    this.previous = this.alive
    this.neighbors = this.getNeighbors()
  }

  draw () {
    if (this.alive) {
      this.board.ctx.fillStyle = this.color
      this.board.ctx.fillRect(this.x, this.y, this.board.scale, this.board.scale)
    }
  }

  // Since we're only using a 1-D array, getting neighboring cells is a little complicated.
  getNeighbors () {
    const col = this.col
    const cols = this.board.columns
    const i = this.i
    const neighbors = []
    const rows = this.board.rows
    const row = this.row

    const n = i - cols
    const e = i + 1
    const s = i + cols
    const w = i - 1

    const nw = n - 1
    const ne = n + 1
    const sw = s - 1
    const se = s + 1

    if (col !== 0) {
      neighbors.push(w)
    }

    if (row !== 0 && col !== 0) {
      neighbors.push(nw)
    }

    if (row !== 0) {
      neighbors.push(n)
    }

    if (row !== 0 && col !== cols - 1) {
      neighbors.push(ne)
    }

    if (col !== cols - 1) {
      neighbors.push(e)
    }

    if (col !== cols - 1 && row !== rows) {
      neighbors.push(se)
    }

    if (row !== rows) {
      neighbors.push(s)
    }

    if (row !== rows && col !== 0) {
      neighbors.push(sw)
    }

    return neighbors
  }

  getLivingNeighbors () {
    const cells = this.board.cells
    const livingNeighbors = []
    const neighbors = this.neighbors

    for (let i = 0, max = neighbors.length; i < max; i++) {
      let cell = cells[neighbors[i]]

      if (cell && cell.previous) {
        livingNeighbors.push(neighbors[i])
      }
    }

    return livingNeighbors
  }

  updatePrevious () {
    this.previous = this.alive
  }

  update () {
    const livingNeighbors = this.getLivingNeighbors().length

    if (this.previous) {
      if (livingNeighbors === 2 || livingNeighbors === 3) {
        this.alive = true
      }

      if (livingNeighbors < 2 || livingNeighbors > 3) {
        this.alive = false
      }
    } else {
      if (livingNeighbors === 3) {
        this.alive = true
      }
    }
  }

  setColors () {
    const fg = this.board.colors.alive.slice(0)
    const bg = this.board.colors.bg

    this.color = hsla(Math.floor(map(this.i, 0, this.board.cells.length, fg[0] - 60, fg[0])), ...fg.slice(1))
    this.bg = hsla(...bg)
  }
}

export default Cell;

